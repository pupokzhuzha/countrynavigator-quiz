package com.countrynavigator.quiz.game.world.app.fieldsOfContainers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.countrynavigator.quiz.game.world.app.R
import com.countrynavigator.quiz.game.world.app.databinding.FragmentMainBinding


class Main : Fragment() {

    private var binding: FragmentMainBinding? = null
    private val mBinding get() = binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.apply {
            start.setOnClickListener {
                findNavController().navigate(R.id.action_main_to_country)
            }
            exit.setOnClickListener {
                activity?.finishAndRemoveTask()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}