package com.countrynavigator.quiz.game.world.app.containers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.countrynavigator.quiz.game.world.app.R
import com.countrynavigator.quiz.game.world.app.replaceActivity

class FirstSide : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_side)

        Handler(Looper.getMainLooper()).postDelayed({
            this.replaceActivity(Head())
        }, 500)
    }
}