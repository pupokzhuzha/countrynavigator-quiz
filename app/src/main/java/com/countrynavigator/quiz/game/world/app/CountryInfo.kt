package com.countrynavigator.quiz.game.world.app

data class CountryInfo(
    val image : Int,
    val tittle : String,
    val first : String,
    val second : String,
    val third : String,
    val rightAnswer : String
)
