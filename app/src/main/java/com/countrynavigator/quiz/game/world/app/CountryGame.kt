package com.countrynavigator.quiz.game.world.app

object CountryGame {

    fun gen(page : Int) : CountryInfo{
        return when(page){
            1 -> CountryInfo(R.drawable.australia, "Rockhampton, Brisbane, Perth", "Australia", "Iceland", "New Zealand", "Australia")
            2 -> CountryInfo(R.drawable.canada, "Winnipeg, Edmonton, Saskatoon", "Ireland", "Denmark", "Canada", "Canada")
            3 -> CountryInfo(R.drawable.kazakhstan, "Rudny, Karaganda, Pavlodar", "Kyrgyzstan", "Kazakhstan", "Moldova", "Kazakhstan")
            4 -> CountryInfo(R.drawable.pakistan, "Bahawalpur, Gujranwala, Multan", "Bangladesh", "India", "Pakistan", "Pakistan")
            5 -> CountryInfo(R.drawable.venezuela, "Maracaibo, Valencia, Caracas", "Bolivia", "Ecuador", "Venezuela", "Venezuela")
            6 -> CountryInfo(R.drawable.zimbabwe, "Bulawayo, Harare, Masvingo", "Madagascar", "Zimbabwe", "Angola", "Zimbabwe")
            7 -> CountryInfo(R.drawable.france, "Lyon, Toulouse, Nantes", "France", "Sierra Leone", "Canada", "France")
            8 -> CountryInfo(R.drawable.austria, "Salzburg, Vienna, Klagenfurt", "Austria", "Germany", "Switzerland", "Austria")
            9 -> CountryInfo(R.drawable.africa, "Pretoria, Cape Town, Durban", "Peru", "South Africa", "Ecuador","South Africa")
            else -> CountryInfo(R.drawable.cuba, "Florida, Ciego de Avila, Guantanamo Bay", "Honduras", "Panama", "Cuba", "Cuba")
        }
    }
}