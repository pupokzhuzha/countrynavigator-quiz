package com.countrynavigator.quiz.game.world.app.containers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.countrynavigator.quiz.game.world.app.R

class Head : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}