package com.countrynavigator.quiz.game.world.app.fieldsOfContainers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.countrynavigator.quiz.game.world.app.CountryGame
import com.countrynavigator.quiz.game.world.app.CountryInfo
import com.countrynavigator.quiz.game.world.app.R
import com.countrynavigator.quiz.game.world.app.databinding.FragmentCountryBinding
import com.countrynavigator.quiz.game.world.app.databinding.FragmentMainBinding
import eightbitlab.com.blurview.BlurView
import eightbitlab.com.blurview.RenderScriptBlur

class Country : Fragment() {

    private var binding: FragmentCountryBinding? = null
    private val mBinding get() = binding!!

    private var page = 1
    private var points = 0
    private lateinit var model : CountryInfo
    private var textBtn = ""
    private lateinit var mBlurView: BlurView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCountryBinding.inflate(layoutInflater, container, false)
        mBinding.apply {
            mBlurView = blur
        }
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        model = CountryGame.gen(page)
        mBinding.apply {
            initListeners()
            modelData()

            first.setOnClickListener {
                textBtn = firstText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    switcherColored(first, true)
                }
                else{
                    switcherColored(first, false)
                }
            }
            second.setOnClickListener {
                textBtn = secondText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    switcherColored(second, true)
                }
                else{
                    switcherColored(second, false)
                }
            }
            third.setOnClickListener {
                textBtn = thirdText.text.toString()
                enableBtns(false)

                if(textBtn == model.rightAnswer){
                    switcherColored(third, true)
                }
                else{
                    switcherColored(third, false)
                }
            }

            next.setOnClickListener {
                page++
                if(page == 11){
                    blurBackground()
                    back.isEnabled = false
                    next.visibility = View.GONE
                    result.text = "$points/10"
                }
                else{
                    enableBtns(true)
                    model = CountryGame.gen(page)
                   modelData()

                    first.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
                    second.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
                    third.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.white)
                }
            }
        }
    }

    private fun switcherColored(card : CardView, boolean: Boolean){
        if (boolean){
            card.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorGreen)
            points++
        }
        else{
            card.backgroundTintList = ContextCompat.getColorStateList(requireActivity(), R.color.colorRed)
        }
    }

    private fun initListeners(){
        mBinding.apply {
            back.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
            toMenu.setOnClickListener {
                activity?.onBackPressedDispatcher?.onBackPressed()
            }
        }
    }

    private fun modelData(){
        mBinding.apply {
            image.setImageResource(model.image)
            tittle.text = model.tittle
            firstText.text = model.first
            secondText.text = model.second
            thirdText.text = model.third
        }
    }

    private fun enableBtns(enable : Boolean){
        if (enable){
            mBinding.apply {
                first.isEnabled = true
                second.isEnabled = true
                third.isEnabled = true
            }
        }
        else{
            mBinding.apply {
                first.isEnabled = false
                second.isEnabled = false
                third.isEnabled = false
            }
        }
    }

    private fun blurBackground() {
        val radius = 8f
        val decorView = activity?.window?.decorView
        val rootView = decorView?.findViewById<ViewGroup>(android.R.id.content)
        val windowBackground = decorView?.background
        if (rootView != null) {
            mBlurView.setupWith(rootView, RenderScriptBlur(requireContext()))
                .setFrameClearDrawable(windowBackground)
                .setBlurRadius(radius)
        }
        mBlurView.visibility = View.VISIBLE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}